<?php

/**
 * @file
 * Administration form include.
 */

/**
 * Form callback to manage search page displays.
 */
function search_page_displays_configure_form($form, &$form_state) {
  $form = array();

  // Get all search pages.
  $search_pages = apachesolr_search_load_all_search_pages();

  $form['help_text'] = array(
    '#markup' => '<p>Set the result view mode to use on each search page.</p>
        <p>Note: If the view mode for all bundles shown on a search page has not been
        configured then the results may not display correctly</p>',
  );

  $form['search_page_displays_pages'] = array(
    '#tree' => TRUE,
  );

  // Get all display modes.
  $entity_info = entity_get_info('node');

  $view_modes = array();
  foreach ($entity_info['view modes'] as $view_name => $view) {
    $view_modes[$view_name] = $view['label'];
  }

  $display_settings = variable_get('search_page_displays_pages', 'search_result');

  foreach ($search_pages as $name => $search_page) {
    $form['search_page_displays_pages'][$name] = array(
      '#title' => $search_page['label'],
      '#type' => 'select',
      '#options' => $view_modes,
      '#default_value' => (isset($display_settings[$name])) ? $display_settings[$name] : 'search_result',
    );
  }

  return system_settings_form($form);
}
